# DevOps Cloud Desafio 1

Projeto destinado à pratica do curso de especialização DevOps da Cloud Treinamentos - módulo Desafio 1. 

Criar um repositório no GitLab, com uma aplicação web qualquer, com instruções para inicializar um container por um dockerfile. 

Criar uma instância EC2 com Jenkins

Criar um job para clonar o repositório, fazer o build da imagem, enviar para o dockerhub, e apagar a imagem da instância.

Este repositório do GitLab ao ser alterado deverá se comunicar com a instância Jenkins por WebHook para execução do Job

Realizado o troubleshooting via Discord com a Turma 3 - DevOps


## Video


## Passo a Passo

- Criar repositório no GitLab


### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.