FROM maven:3-jdk-8-alpine

WORKDIR /home/ubuntu/devopsclouddesafio1

COPY . /home/ubuntu/devopsclouddesafio1

ENV PORT 5000

EXPOSE $PORT